Rails.application.routes.draw do
  get "/" => "home#top"
  get "home/top" => 'home#top'
  get "new" => "home#new"
  post "home/create" => "home#create"
  get "home/destroy" => "home#destroy"
  get "home/:id/destroy" => "home#destroy"
end