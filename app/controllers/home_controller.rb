class HomeController < ApplicationController
  def top
    @products=Product.all
  end
  
  def new
  end
  
  def create
    @product=Product.new(name: params[:name])
    @product.save
    redirect_to("/home/top")
  end
  
  def destroy
    @product=Product.find_by(id: params[:id])
    @product.destroy
    redirect_to("/home/top")
  end 
end
